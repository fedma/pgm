#ifndef PGMAIMAGE_H

#include <fstream>
#include <string>
#include <iterator>
#include "image.h"

class pgmImage : public image<std::uint8_t>
{
private:
	std::string filename;

protected:
	std::string magicNumber;
	std::string comment;
	const std::string s_255 = "255";
	const std::string s_P5 = "P5";
	const std::string s_P2 = "P2";

	void writeHeader (std::ofstream& ofs)
	{
		writeHeader (ofs, magicNumber, comment);
	}

	void writeHeader (std::ofstream& ofs, const std::string& magicNumber)
	{
		writeHeader (ofs, magicNumber, comment);
	}

	void writeHeader (std::ofstream& ofs, const std::string& magicNumber, const std::string& comment)
	{
		ofs << magicNumber << '\n' << '#' << comment << '\n' << width << ' ' << heigth << '\n' << s_255 << '\n';
	}

public:
	pgmImage () : image<std::uint8_t> () {}
	pgmImage (std::uint32_t width_, std::uint32_t heigth_) : image<std::uint8_t> (width_, heigth_) {}
	pgmImage (const char* filename_) : filename (filename_)
	{
		std::ifstream ifs (filename, std::ios::binary);
		std::string tmp255;
		char tmpNL, tmpHash;

		if (!ifs)
		{
			throw std::invalid_argument ("Cannot open specified file.");
		}

		ifs >> magicNumber;

		if (magicNumber.compare (s_P2) != 0 && magicNumber.compare (s_P5) != 0)
		{
			throw std::exception ("Invalid magic number.");
		}

		ifs.get (tmpNL);
		ifs.get (tmpHash);

		if (tmpHash == '#')
		{
			std::getline (ifs, comment, '\n');
		}
		else
		{
			ifs.seekg (-2, std::ios::cur);
		}

		ifs >> width;
		ifs >> heigth;
		ifs >> tmp255;

		if (tmp255.compare (s_255) != 0)
		{
			throw std::exception ("Invalid terminating number.");
		}

		char cacca = ifs.get ();

		if (magicNumber.compare (s_P2) == 0)
		{
			uint16_t tmpPixel;

			while (ifs >> std::dec >> tmpPixel)
			{
				data.push_back (static_cast<uint8_t>(tmpPixel));
			}
		}
		else
		{
			uint8_t tmpPixel;

			/*while (ifs >> std::noskipws >> tmpPixel)
			{
				data.push_back (tmpPixel);
			}*/

			while (ifs.read (reinterpret_cast<char*>(&tmpPixel), 1))
			{
				data.push_back (tmpPixel);
			}
		}
	}

	void save ()
	{
		save (filename);
	}

	void savePlain ()
	{
		savePlain (filename);
	}

	void save (std::ofstream& ofs)
	{
		writeHeader (ofs, s_P5);

		for (char pixel : data)
		{
			ofs.write (&pixel, 1);
		}
	}

	void savePlain (std::ofstream& ofs)
	{
		writeHeader (ofs, s_P2);

		for (uint16_t pixel : data)
		{
			ofs << std::dec << pixel << ' ';
		}
	}

	void save (const std::string& filename)
	{
		std::ofstream ofs (filename, std::ios::binary);

		if (!ofs)
		{
			throw std::invalid_argument ("Invalid filename. Could not save pgmImage.");
		}

		save (ofs);
	}

	void savePlain (const std::string& filename)
	{
		std::ofstream ofs (filename, std::ios::binary);

		if (!ofs)
		{
			throw std::invalid_argument ("Invalid filename. Could not save pgmImage.");
		}

		savePlain (ofs);
	}

	void makeGradient ()
	{
		std::uint8_t value = 0;
		std::uint32_t allPixels = width * heigth;

		for (std::uint32_t i = 0; i < allPixels; i++)
		{
			if (i % width == 0 && i != 0)
				value++;
			data[i] = value;
		}
	}
};

#endif /*PGMAIMAGE_H*/