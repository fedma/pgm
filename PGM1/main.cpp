#include <iostream>

#include "pgmImage.h"

int main (int argc, char** argv)
{
	std::string plainFileName = "rana_bin.pgm";

	pgmImage myImage (plainFileName.c_str());

	myImage.flipVertical ();

	myImage.save ("rana_bin_flipped.pgm");
}