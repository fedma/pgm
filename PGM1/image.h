#ifndef IMAGE_H

#include <cstdint>
#include <vector>

template<class pixel_t>
class image
{
protected:
	std::uint32_t width;
	std::uint32_t heigth;	
	std::vector<pixel_t> data;

public:
	image () : width (0), heigth (0), data (0) {}
	image (std::uint32_t width_, std::uint32_t heigth_) : width (width_), heigth (heigth_), data (width * heigth) {}
	image (const std::vector<pixel_t>& img, std::uint32_t width_, std::uint32_t heigth_) : width (width_), heigth (heigth_), data (img) {}


	void flipVertical ()
	{
		uint32_t firstIndex = 0, secondIndex = width * heigth - width, rowCount = 1;

		while (firstIndex < secondIndex)
		{
			std::swap (data[firstIndex], data[secondIndex]);
			firstIndex++;
			secondIndex++;
			if (secondIndex % width == 0)
			{
				rowCount++;
				secondIndex = width * heigth - width * rowCount;
			}
		}
	}

	bool dataEquals (const image& other)
	{
		if (other.data.size () != data.size ())
		{
			return false;
		}

		for (uint32_t i = 0; i < data.size (); i++)
		{
			if (data.at (i) != other.data.at (i))
				return false;
		}

		return true;
	}
};

#endif /*IMAGE_H*/